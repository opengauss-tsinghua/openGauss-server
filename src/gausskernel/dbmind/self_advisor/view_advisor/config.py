#!/usr/bin/python

["database_connection"]

local_pg = {
    "host": "localhost",
    "database": "",
    "user": "",
    "password": ""
}


["encoder_reducer"]

enc_rdc_model_path = "./result/job/encoder_reducer/2021.4.27-1/"
enc_rdc_trainset_dir = "./dataset/JOB/trainset/"


["DQN"]

dqn_budget = 500
